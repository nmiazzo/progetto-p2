# Lettura obbligatoria
## Disclaimer
Questo progetto è stato realizzato per il corso di PAO nell'anno accademico 2018-2019. Il progetto è da considerarsi solo come esempio non esaustivo. Consiglio di leggere la relazione.

## Criticità
Ho deciso di dividere i file in 2 cartelle chiamate ```gui``` e ```core```, tuttavia ciò ha portato a problemi a livello di consegna. Ho scoperto in seguito che il comando ```consegna progetto-pao-2019``` utilizzato nel mio anno l'invio dei file è programmato per ignorare file e cartelle contenenti la parola ```core``` in quanto potrebbe trattarsi di file _coredump_ usati dal sistema. Invito caldamente (se si utilizzerà ancora la consegna da terminale) di non utilizzare tale nome

### Domande
Resto a disposizione per eventuali domande:
- Telegram: @nmiazzo
